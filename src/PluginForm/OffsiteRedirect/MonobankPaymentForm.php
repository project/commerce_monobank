<?php

namespace Drupal\commerce_monobank\PluginForm\OffsiteRedirect;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides the class for payment off-site form.
 *
 * Provide a buildConfigurationForm() method which calls buildRedirectForm()
 * with the right parameters.
 */
class MonobankPaymentForm extends BasePaymentOffsiteForm {

    use StringTranslationTrait;
    const TEST_API_URL = 'https://api.monobank.ua/';
    const CCY = 980;

    /**
     * Gateway plugin.
     *
     * @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface
     */
    private $paymentGatewayPlugin;

    /**
     * Getting plugin's configuration.
     *
     * @param string $configuration
     *   Configuration name.
     *
     * @return mixed
     *   Configuration value.
     */
    private function getConfiguration($configuration) {
        return $this->paymentGatewayPlugin->getConfiguration()[$configuration];
    }

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
        $form = parent::buildConfigurationForm($form, $form_state);

        /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
        $payment = $this->entity;
        $this->paymentGatewayPlugin = $payment->getPaymentGateway()->getPlugin();

        $order        = $payment->getOrder();
        $orderId      = $order->id();

        if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] == 'https://pay.monobank.ua/') {
            $message = t('Client returned from MonoBank, order #@order_id', ['@order_id' => $order->id()]);
            \Drupal::logger('commerce_monobank')->info($message);

            //check status
            $this->checkPaymentStatus($order);

            $response = new RedirectResponse('/checkout/'.$orderId.'/complete');
            $response->send();
        }

        $total_price  =  (int) ($order->getTotalPrice()->getNumber() * 100 );


        $api_url      = $this->getApiUrl();
        $host         = \Drupal::request()->getHost();
        $redirect_url = 'https://'.$host.'/checkout/'.$orderId.'/complete';
        $token        = (string) $this->getConfiguration('x_token');

        $data = [
            "amount" => $total_price,
            "ccy" => $this::CCY,
            "redirectUrl"=> $redirect_url,
            "validity"=> (int) $this->getConfiguration('validity'),
            "paymentType"=> $this->getConfiguration('payment_type'),
        ];




        if ($order) {
            $info = [
                'reference' => $order->id(),
            ];
            foreach ($order->getItems() as $item) {

              $sum = (int) ($item->getUnitPrice()->getNumber() * 100);

              $adjustments = $item->getAdjustments();
              if (!empty($adjustments)) {
                $percentage = $adjustments[0]->getPercentage();
                $sum = $sum - $sum * $percentage;
              }

              $info['basketOrder'][] = [
                  'name' => $item->getTitle(),
                  'qty' => (int) $item->getQuantity(),
                  'sum' => $sum,
                  'code' => $item->id(),
                  'icon' => '',
              ];
            }
            $data['merchantPaymInfo'] = $info;
        }

        try {
            $client = \Drupal::httpClient();
            $response = $client->post($api_url.'api/merchant/invoice/'.'create', [
                'body' => json_encode($data),
                'headers' => [
                    'Content-Type' => 'application/json',
                    'X-Token' => trim($token),
                    'X-Cms' => 'Drupal CMS',
                    'X-Cms-Version' => '9'
                ],
            ]);
            $resData = $response->getBody()->getContents();
            if(!empty($resData)) {
                $resData = @json_decode($resData, TRUE);
                $payUrl = $resData['pageUrl'];

                $connection = \Drupal::service('database');
                $query = $connection->select('commerce_order', 'o')
                    ->condition('o.order_id', $orderId, '=')
                    ->fields('o', ['field_invoiceid_monobank'])
                    ->range(0, 1);
                $result = $query->execute()->fetchAll();

                if($result[0]->field_invoiceid_monobank == '') {
                    $connection->merge('commerce_order')
                        ->key('order_id', $orderId)
                        ->fields(['field_invoiceid_monobank' => $resData['invoiceId']])
                        ->execute();
                }

                $response = new RedirectResponse($payUrl);
                if($response->send()) {
                    $message = t('The client was returned to Monobank, order #@order_id', ['@order_id' => $order->id()]);
                    \Drupal::messenger()->addMessage($message, 'status');
                    \Drupal::logger('commerce_monobank')->info($message);
                }
            }
        }
        catch ( \Exception $e) {
            $message =$e->getMessage();
            \Drupal::messenger()->addMessage( $message , 'error');
            \Drupal::logger('commerce_monobank')->error($message);
        }
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function getApiUrl() {
        if($this->getConfiguration('mode') == 'live') {
            $action_url = $this->getConfiguration('action_url');
            return $action_url;
        }
        else {
            return $this::TEST_API_URL;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function savePayment (OrderInterface $order, $state){
        $payment_gateway = $order->get('payment_gateway')->getValue()[0]['target_id'];
        $payment = Payment::create([
            'type' => 'payment_default',
            'payment_gateway' => $payment_gateway,
            'order_id' => $order->id(),
            'amount' => $order->getTotalPrice(),
            'state' => $state,
        ]);

        if($payment->save()){
            return TRUE;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function saveorder (OrderInterface $order, $state){
        $order->set('state', $state);
        if($order->save()){
            return TRUE;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function checkPaymentStatus($order) {
        $orderId      = $order->id();

        $connection = \Drupal::service('database');
        $query = $connection->select('commerce_order', 'o')
            ->condition('o.order_id', $orderId, '=')
            ->fields('o', ['field_invoiceid_monobank'])
            ->range(0, 1);
        $result = $query->execute()->fetchAll();
        $invoiceid = $result[0]->field_invoiceid_monobank;

        $api_url      = $this->getApiUrl();
        $token        = (string) $this->getConfiguration('x_token');

        try {
            $client = \Drupal::httpClient();
            $response = $client->get($api_url.'api/merchant/invoice/status?invoiceId='.$invoiceid, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'X-Token' => trim($token),
                    'X-Cms' => 'Drupal CMS',
                    'X-Cms-Version' => '9'
                ],
            ]);
            $resData = $response->getBody()->getContents();
            if(!empty($resData)) {
                $resData = @json_decode($resData, TRUE);


                if($resData['status'] == 'success') {
                    if($this->saveOrder($order, 'new')) {
                        $message = t('Order #@order_id has been edited(status "New")', ['@order_id' => $order->id()]);
                        \Drupal::logger('commerce_monobank')->info($message);

                        $this->getFiscalChecks($invoiceid, $order);
                    }
                    if($this->savePayment($order, 'complete')) {
                        $message = t('Order #@order_id has been edited(payment "completed")', ['@order_id' => $order->id()]);
                        \Drupal::logger('commerce_monobank')->info($message);
                    }
                    return TRUE;
                }
                if($resData['status'] !== 'success') {
                    $message = 'Error payment. Order #'.$orderId.'. ';
                    foreach ($resData as $key => $value) {
                        $message .= $key . '=' . $value. '; ';
                    }
                    \Drupal::messenger()->addMessage($message, 'warning');
                    \Drupal::logger('commerce_monobank')->warning($message);
                }
            }
        }
        catch ( \Exception $e) {
            \Drupal::messenger()->addMessage( $e->getMessage() , 'error');
        }
    }
    /**
     * {@inheritdoc}
     */
    public function getFiscalChecks ($invoiceid, $order) {
        $api_url      = $this->getApiUrl();
        $token        = (string) $this->getConfiguration('x_token');
        $orderId      = $order->id();

        try {
            $client = \Drupal::httpClient();
            $response = $client->get($api_url.'api/merchant/invoice/fiscal-checks?invoiceId='.$invoiceid, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'X-Token' => trim($token),
                    'X-Cms' => 'Drupal CMS',
                    'X-Cms-Version' => '9'
                ],
            ]);
            $resData = $response->getBody()->getContents();
            if(!empty($resData)) {
                $resData = @json_decode($resData, TRUE);

                if($resData['checks'][0]) {
                    $this->sendEmail($resData['checks'][0]);
                }
                if($resData['status'] !== 'success') {
                    $message = 'Fiscal receipt not found. Order #'.$orderId.'. ';
                    foreach ($resData as $key => $value) {
                        $message .= $key . '=' . $value. '; ';
                    }
                    \Drupal::messenger()->addMessage($message, 'warning');
                    \Drupal::logger('commerce_monobank')->warning($message);
                }
            }
        }
        catch ( \Exception $e) {
            \Drupal::messenger()->addMessage( $e->getMessage() , 'error');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function sendEmail ($resData) {
        $user = \Drupal::currentUser();
        $url = $resData['taxUrl'];

        $params = [
            'email' => \Drupal::config('system.site')->get('mail'),
            'subject' => t('Fiscal receipt of the order'),
            'text' => t('View your electronic receipt - <a href="@url">here</a>.', ['@url' => $url]),
            'user' => $user,
        ];

        $result = \Drupal::service('plugin.manager.mail')->mail(
            'commerce_monobank',
            'commerce_monobank_complete_order',
            $user->getEmail(),
            \Drupal::currentUser()->getPreferredLangcode(),
            $params,
            NULL,
            TRUE
        );

        if ($result['result'] !== true) {
            $message = t('There was a problem sending your message and it was not sent.');
            \Drupal::logger('commerce_monobank')->warning($message);
        } else {
            $message = t('Your message has been sent.');
            \Drupal::logger('commerce_monobank')->info($message);
        }
    }

}

