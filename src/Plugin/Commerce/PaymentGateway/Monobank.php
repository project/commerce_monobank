<?php

namespace Drupal\commerce_monobank\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsNotificationsInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the Off-site Redirect payment Monobank gateway.
 *
 * @CommercePaymentGateway(
 *   id = "monobank_offsite_gateway",
 *   label = "Monobank Commerce",
 *   display_label = "Monobank Commerce",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_monobank\PluginForm\OffsiteRedirect\MonobankPaymentForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class Monobank extends OffsitePaymentGatewayBase implements SupportsNotificationsInterface {

    use StringTranslationTrait;

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration() {
        $config   = \Drupal::config('monobank.config');
        $defaults = [];

        $defaults = $defaults + [
                'mode'          => $config->get('mode'),
                'x_token'       => $config->get('monobank.x_token'),
                'validity'      => $config->get('monobank.validity'),
                'payment_type'  => $config->get('monobank.payment_type'),
                'action_url'    => $config->get('monobank.action_url'),
            ];
        return $defaults + parent::defaultConfiguration();
    }

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
        $form = parent::buildConfigurationForm($form, $form_state);

        $form['x_token'] = [
            '#type' => 'textfield',
            '#title' => t('X-Token'),
            '#default_value' => $this->configuration['x_token'],
            '#required' => TRUE,
            '#description' => t('Token from personal account https://web.monobank.ua/ or test token from https://api.monobank.ua/'),
        ];
        $form['validity'] = [
            '#type' => 'number',
            '#title' => t('validity time'),
            '#default_value' => $this->configuration['validity'],
            '#required' => TRUE,
            '#description' => t('The validity period is in seconds, by default the account expires after 24 hours, example "3600"'),
            '#min' => 60,
            '#attributes' => [
                'min' => '60',
            ],
        ];
        $form['payment_type'] = [
            '#type' => 'select',
            '#title' => t('Payment Type'),
            '#options' => [
                'debit' => t('debit'),
                'hold' => t('hold'),
            ],
            '#default_value' => $this->configuration['payment_type'],
            '#required' => TRUE,
            '#description' => t('Example "debit"'),
        ];
        $form['action_url'] = [
            '#type' => 'textfield',
            '#title' => t('Action url'),
            '#default_value' => $this->configuration['action_url'],
            '#required' => TRUE,
            '#description' => t('API URL monobank, Its probably "https://api.monobank.ua/"'),
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
        parent::validateConfigurationForm($form, $form_state);

        $values = $form_state->getValue($form['#parents']);
        $required_fields = [
            'public_key',
            'private_key',
            'description',
            'api_url',
            'action_url',
        ];
        foreach ($required_fields as $key) {
            if (empty($values[$key])) {
                $this->messenger()->addError($this->t('Monobank service is not configured for use. Please contact an administrator to resolve this issue.'));
                return FALSE;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
        parent::submitConfigurationForm($form, $form_state);

        if (!$form_state->getErrors()) {
            $values                                 = $form_state->getValue($form['#parents']);
            $this->configuration['mode']            = $values['mode'];
            $this->configuration['x_token']         = $values['x_token'];
            $this->configuration['validity']        = $values['validity'];
            $this->configuration['payment_type']    = $values['payment_type'];
            $this->configuration['action_url']      = $values['action_url'];

        }
    }
}
